export default [
  {
    url: '/merchantgoodspicture',
    type: 'post',
    response: () => {
      return {
        status:200,
        msg:"上传成功!",
        data:{
          src:["https://imgs.juanpao.com/merchant%2Fshop%2Fgoods_picture%2F443%2F000802%2F15734410365dc8ce0ce530a.png"]
        }
      }
    }
  },
  {
    url: '/merchantgoodspicture/*',
    type: 'delete',
    response: () => {
      return {
        status: 200,
        message: '删除成功!'
      }
    }
  },
  {
    url: '/pictureGroup',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        count: 3,
        data: [
          {"id":"355","key":"000802","merchant_id":"443","supplier_id":"0","name":"相册一","status":"1","create_time":"1572336444","update_time":"1572336444","delete_time":null,"format_create_time":"2019-10-29 16:07:24","format_update_time":"2019-10-29 16:07:24","number":60},
          {"id":"354","key":"000802","merchant_id":"443","supplier_id":"0","name":"相册二","status":"1","create_time":"1572335671","update_time":"1572335671","delete_time":null,"format_create_time":"2019-10-29 15:54:31","format_update_time":"2019-10-29 15:54:31","number":102},
          {"id":"353","key":"000802","merchant_id":"443","supplier_id":"0","name":"相册三","status":"1","create_time":"1572335142","update_time":"1572335142","delete_time":null,"format_create_time":"2019-10-29 15:45:42","format_update_time":"2019-10-29 15:45:42","number":150}
        ]
      }
    }
  },
  {
    url: '/picturegroup',
    type: 'post',
    response: () => {
      return {
        status: 200,
        message: '添加成功',
      }
    }
  },
  {
    url: '/picturegroup/*',
    type: 'put',
    response: () => {
      return {
        status: 200,
        message: '添加成功',
      }
    }
  },
  {
    url: '/picturegroup/*',
    type: 'delete',
    response: () => {
      return {
        status: 200,
        message: '删除成功',
      }
    }
  },
  {
    url: '/picture/0',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        count: 1,
        data: [
          {"id":"7542","key":"000802","merchant_id":"443","supplier_id":"0","picture_group_id":"354","name":"商品分组.png","width":"1876.00","height":"844.00","pic_url":"images/2.jpg","md5":"dd59a1ac7ba34e23d3bb117d9c979cb4","status":"1","create_time":"1572335728","update_time":"1572335728","delete_time":null,"format_create_time":"2019-10-29 15:55:28","format_update_time":"2019-10-29 15:55:28"}
        ]
      }
    }
  },
  {
    url: '/picture/355',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        count: 1,
        data: [
          {"id":"7594","key":"000802","merchant_id":"443","supplier_id":"0","picture_group_id":"354","name":"商品分组.png","width":"1876.00","height":"844.00","pic_url":"images/2.jpg","md5":"dd59a1ac7ba34e23d3bb117d9c979cb4","status":"1","create_time":"1572335728","update_time":"1572335728","delete_time":null,"format_create_time":"2019-10-29 15:55:28","format_update_time":"2019-10-29 15:55:28"}
        ]
      }
    }
  },
  {
    url: '/picture/354',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        count: 1,
        data: [
          {"id": "7771","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增商品分组.png","width": "1870.00","height": "843.00","pic_url": "images/1.jpg","md5": "ece5ec9168f9fd9707988d06693d8497","status": "1","create_time": "1572425509","update_time": "1572425509","delete_time": null,"format_create_time": "2019-10-30 16:51:49","format_update_time": "2019-10-30 16:51:49"},
          {"id": "7770","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增区域分组.png","width": "1873.00","height": "932.00","pic_url": "images/2.jpg","md5": "2a1d23eee40611693838e2a8f37cbb7b","status": "1","create_time": "1572423693","update_time": "1572423693","delete_time": null,"format_create_time": "2019-10-30 16:21:33","format_update_time": "2019-10-30 16:21:33"},
          {"id": "7593","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "商品分组.png","width": "1876.00","height": "844.00","pic_url": "images/3.jpg","md5": "dd59a1ac7ba34e23d3bb117d9c979cb4","status": "1","create_time": "1572335728","update_time": "1572335728","delete_time": null,"format_create_time": "2019-10-29 15:55:28","format_update_time": "2019-10-29 15:55:28"},
          {"id": "7773","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增商品分组.png","width": "1870.00","height": "843.00","pic_url": "images/4.jpg","md5": "ece5ec9168f9fd9707988d06693d8497","status": "1","create_time": "1572425509","update_time": "1572425509","delete_time": null,"format_create_time": "2019-10-30 16:51:49","format_update_time": "2019-10-30 16:51:49"},
          {"id": "7774","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增区域分组.png","width": "1873.00","height": "932.00","pic_url": "images/5.jpg","md5": "2a1d23eee40611693838e2a8f37cbb7b","status": "1","create_time": "1572423693","update_time": "1572423693","delete_time": null,"format_create_time": "2019-10-30 16:21:33","format_update_time": "2019-10-30 16:21:33"},
          {"id": "7595","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "商品分组.png","width": "1876.00","height": "844.00","pic_url": "images/6.jpg","md5": "dd59a1ac7ba34e23d3bb117d9c979cb4","status": "1","create_time": "1572335728","update_time": "1572335728","delete_time": null,"format_create_time": "2019-10-29 15:55:28","format_update_time": "2019-10-29 15:55:28"},
          {"id": "7776","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增商品分组.png","width": "1870.00","height": "843.00","pic_url": "images/7.jpg","md5": "ece5ec9168f9fd9707988d06693d8497","status": "1","create_time": "1572425509","update_time": "1572425509","delete_time": null,"format_create_time": "2019-10-30 16:51:49","format_update_time": "2019-10-30 16:51:49"},
          {"id": "7777","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增区域分组.png","width": "1873.00","height": "932.00","pic_url": "images/8.jpg","md5": "2a1d23eee40611693838e2a8f37cbb7b","status": "1","create_time": "1572423693","update_time": "1572423693","delete_time": null,"format_create_time": "2019-10-30 16:21:33","format_update_time": "2019-10-30 16:21:33"},
          {"id": "7598","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "商品分组.png","width": "1876.00","height": "844.00","pic_url": "images/9.jpg","md5": "dd59a1ac7ba34e23d3bb117d9c979cb4","status": "1","create_time": "1572335728","update_time": "1572335728","delete_time": null,"format_create_time": "2019-10-29 15:55:28","format_update_time": "2019-10-29 15:55:28"},
          {"id": "7779","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增商品分组.png","width": "1870.00","height": "843.00","pic_url": "images/10.jpg","md5": "ece5ec9168f9fd9707988d06693d8497","status": "1","create_time": "1572425509","update_time": "1572425509","delete_time": null,"format_create_time": "2019-10-30 16:51:49","format_update_time": "2019-10-30 16:51:49"},
          {"id": "7710","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增区域分组.png","width": "1873.00","height": "932.00","pic_url": "images/11.jpg","md5": "2a1d23eee40611693838e2a8f37cbb7b","status": "1","create_time": "1572423693","update_time": "1572423693","delete_time": null,"format_create_time": "2019-10-30 16:21:33","format_update_time": "2019-10-30 16:21:33"},
          {"id": "7511","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "商品分组.png","width": "1876.00","height": "844.00","pic_url": "images/12.jpg","md5": "dd59a1ac7ba34e23d3bb117d9c979cb4","status": "1","create_time": "1572335728","update_time": "1572335728","delete_time": null,"format_create_time": "2019-10-29 15:55:28","format_update_time": "2019-10-29 15:55:28"},
          {"id": "7712","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增商品分组.png","width": "1870.00","height": "843.00","pic_url": "images/13.jpg","md5": "ece5ec9168f9fd9707988d06693d8497","status": "1","create_time": "1572425509","update_time": "1572425509","delete_time": null,"format_create_time": "2019-10-30 16:51:49","format_update_time": "2019-10-30 16:51:49"},
          {"id": "7713","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "新增区域分组.png","width": "1873.00","height": "932.00","pic_url": "images/14.jpg","md5": "2a1d23eee40611693838e2a8f37cbb7b","status": "1","create_time": "1572423693","update_time": "1572423693","delete_time": null,"format_create_time": "2019-10-30 16:21:33","format_update_time": "2019-10-30 16:21:33"},
          {"id": "7514","key": "000802","merchant_id": "443","supplier_id": "0","picture_group_id": "354","name": "商品分组.png","width": "1876.00","height": "844.00","pic_url": "images/15.jpg","md5": "dd59a1ac7ba34e23d3bb117d9c979cb4","status": "1","create_time": "1572335728","update_time": "1572335728","delete_time": null,"format_create_time": "2019-10-29 15:55:28","format_update_time": "2019-10-29 15:55:28"},
        ]
      }
    }
  },
  {
    url: '/picture/353',
    type: 'get',
    response: () => {
      return {
        status: 200,
        message: '请求成功',
        count: 1,
        data: [
          {"id":"7596","key":"000802","merchant_id":"443","supplier_id":"0","picture_group_id":"354","name":"商品分组.png","width":"1876.00","height":"844.00","pic_url":"images/2.jpg","md5":"dd59a1ac7ba34e23d3bb117d9c979cb4","status":"1","create_time":"1572335728","update_time":"1572335728","delete_time":null,"format_create_time":"2019-10-29 15:55:28","format_update_time":"2019-10-29 15:55:28"}
        ]
      }
    }
  }
]

