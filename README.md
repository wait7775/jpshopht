# 微信微商城小程序系统后台前端element+vue.js

### 如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！<br>

#### 介绍
由于jpshop微商城系统是前后端分离开发，所以代码分开仓库下载<br>
这个代码是jpshop微商城系统的后台前端，采用element+vue.js开发，上线使用需要打包才能访问<br>
后端文件到下面的链接下载：https://gitee.com/jpshop/jpshop
QQ群：1029489867，欢迎交流<br>

支持个人使用，允许商业使用<br>
使用期间需保留我公司商标，logo，版权等说明，不能进行二次出售，请自觉遵守使用协议<br>
如需去版权标志，可以购买全功能商业版<br>

#### 软件架构
软件架构说明<br>
后端采用Yii2框架：一个高性能电商专用框架，用于快速开发现代Web应用程序<br>
使用element+vue.js开发 <br>

体验地址 http://an.juanpao.cn   帐号：admin  密码：123456

![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/101331_cdbbdb66_1843738.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/101343_b2011889_1843738.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/101355_7de75117_1843738.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0214/101404_27f15fc9_1843738.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0317/190543_61cec198_1843738.jpeg "1_03.jpg")