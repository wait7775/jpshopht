import store from '@/store'
import { getGoodsGroup } from '@/api/goods'

export default {
  data() {
    return {
      goodsGroup: []
    }
  },
  methods: {
    getGoodsGroup(page) {
      const params = {
        key: store.state.app.activeApp.saa_key,
        limit: 10,
        page: page
      }
      getGoodsGroup(params).then(response => {

      })
    }
  },
}