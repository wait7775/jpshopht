import request from '@/utils/request'

/**
 * 获取概览数据
 * @param {*} params 
 */
export function getShopTotal(params) {
  return request({
    url: '/merchantShopTotal',
    method: 'get',
    params
  })
}

//升级日志

export function getUpLog(params) {
  return request({
    url: '/merchantVersion',
    method: 'get',
    params
  })
}

//升级数据库
export function upsql() {
  return request({
    url: '/shop/test/update',
    method: 'get'
  })
}